import java.util.*;
import java.lang.*;
import java.io.*;

class GFG {
	public static void main(String[] args) throws IOException
	{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine().trim());
        while(t-->0)
        {
            StringTokenizer stt = new StringTokenizer(br.readLine());
            
            int n = Integer.parseInt(stt.nextToken());
            int m = Integer.parseInt(stt.nextToken());
            int k = Integer.parseInt(stt.nextToken());
            int a[] = new int[(int)(n)];
            int b[] = new int[(int)(m)];
            
            
            String inputLine[] = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                a[i] = Integer.parseInt(inputLine[i]);
            }
            String inputLine1[] = br.readLine().trim().split(" ");
            for (int i = 0; i < m; i++) {
                b[i] = Integer.parseInt(inputLine1[i]);
            }
            
            
            Solution obj = new Solution();
            System.out.println(obj.kthElement( a, b, n, m, k));
            
        }
	}
}


class Solution {
    public long kthElement(int arr1[], int arr2[], int n, int m, int k) {
        int left = Math.max(0, k - m); // Adjusting left boundary for binary search
        int right = Math.min(k, n); // Adjusting right boundary for binary search
        
        while (left < right) {
            int mid = left + (right - left) / 2;
            int count = mid; // Count elements from arr1
            
            if (count < k && k - count <= m && arr1[count] >= arr2[k - count - 1]) {
                right = mid; // Move to the left
            } else if (count < k && k - count > 0 && arr1[count] < arr2[k - count - 1]) {
                left = mid + 1; // Move to the right
            } else {
                right = mid; // Move to the left
            }
        }
        
        // Check for boundary cases and return the kth element
        if (left == 0) return arr2[k - 1];
        if (left == k) return arr1[k - 1];
        return Math.max(arr1[left - 1], arr2[k - left - 1]);
    }
}
